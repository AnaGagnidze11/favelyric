![Fave Lyric Logo](https://lh3.googleusercontent.com/znzyLg5OEdcn9svFdPv35rNQsvc5qUz7v0dH8c_yARvE5BQ6tkPggUhQZvOL8CrOFadT3Ao=s85)

# Fave Lyric
Fave Lyric is an app for searching the lyrics of your favorite songs.



## Motivation behind a project
This project is made for an TBC Bootcamp assignment. Creator greatly enjoys listening to music and picking apart its lyrics, therefore they made this android application to easily search up the lyrics of the songs. If you're ever with your friends, singing and having fun, you can use this app and find the lyrics of you favourite songs.

## How to use it
After registering to the application, only thing you will need to do is to input the artist's or band's name and the title of the song! After clicking the search button, app finds the lyrics as soon as possible and displays it for you.


## Platform and language used.
Android Studio, Kotlin


## Requirements
At this point, you only need Android Studio to run this application and see the existing functionalities. 


## What  to expect
* Fave Lyric has a registration page. In which user can choose to either Log in, or Sign up. (Both those pages have their own pages)
* After that, user comes across a Search page, where they input desired song's title and the artist.
* Lastly, After server returns the lyrics, it is displayed on another screen, where the user can read it and use it to sing along to their favorite song!

## Contributors
Ana Gagnidze
