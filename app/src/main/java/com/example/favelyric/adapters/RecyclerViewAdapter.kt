package com.example.favelyric.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.favelyric.R
import com.example.favelyric.api.LyricModel
import com.example.favelyric.databinding.LyricHolderBinding

class RecyclerViewAdapter : RecyclerView.Adapter<RecyclerViewAdapter.ItemViewHolder>() {

    private var itemList = emptyList<LyricModel>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.lyric_holder,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = itemList.size


    inner class ItemViewHolder(private val binding: LyricHolderBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private lateinit var lyric: LyricModel
        fun bind() {
            lyric = itemList[adapterPosition]
            binding.lyrics = lyric.lyrics
        }
    }


    fun setData(items: MutableList<LyricModel>) {
        this.itemList = items
        notifyDataSetChanged()
    }
}