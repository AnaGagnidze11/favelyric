package com.example.favelyric.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.example.favelyric.R
import com.example.favelyric.databinding.FragmentAuthenticationBinding
import com.google.firebase.auth.FirebaseAuth

class AuthenticationFragment : Fragment() {

    private var _binding: FragmentAuthenticationBinding? = null
    private val binding get() = _binding!!

    private lateinit var auth: FirebaseAuth

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DataBindingUtil.inflate(inflater, R.layout.fragment_authentication, container, false)
        auth = FirebaseAuth.getInstance()
        openAuth()
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        init()
    }

    private fun init(){
        val currentUser = auth.currentUser
        if (currentUser != null){
            findNavController().navigate(R.id.action_authenticationFragment_to_searchFragment)
        }
    }

    private fun openAuth(){
        binding.setOnSignUpClicked {
            findNavController().navigate(R.id.action_authenticationFragment_to_signUpFragment)
        }

        binding.setOnLogInClicked {
            findNavController().navigate(R.id.action_authenticationFragment_to_logInFragment)
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}