package com.example.favelyric.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.favelyric.R
import com.example.favelyric.adapters.RecyclerViewAdapter
import com.example.favelyric.api.LyricModel
import com.example.favelyric.databinding.FragmentLyricBinding

class LyricFragment : Fragment() {

    private var _binding: FragmentLyricBinding? = null
    private val binding get() = _binding!!

    private lateinit var adapter: RecyclerViewAdapter

    private val lyricList: MutableList<LyricModel> = mutableListOf()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DataBindingUtil.inflate(inflater, R.layout.fragment_lyric, container, false)

        init()

        return binding.root
    }

    private fun init(){
        val bandName = arguments?.getString("Band")
        val title = arguments?.getString("Song")
        val lyrics = arguments?.getParcelable<LyricModel>("Lyric")

        binding.artist = bandName
        binding.song = title
        if (lyrics != null) {
            lyricList.add(lyrics)
        }

        initRecycler()

        adapter.setData(lyricList)


    }

    private fun initRecycler(){

        adapter = RecyclerViewAdapter()
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.adapter = adapter

    }




    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}