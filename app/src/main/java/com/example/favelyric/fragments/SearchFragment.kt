package com.example.favelyric.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.favelyric.R
import com.example.favelyric.api.ResultControl
import com.example.favelyric.databinding.FragmentSearchBinding
import com.example.favelyric.viewmodel.LyricViewModel
import com.google.firebase.auth.FirebaseAuth

class SearchFragment : Fragment() {

    private var _binding: FragmentSearchBinding? = null
    private val binding get() = _binding!!

    private val viewModel: LyricViewModel by viewModels()

    private lateinit var auth: FirebaseAuth

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search, container, false)
        auth = FirebaseAuth.getInstance()
        init()

        return binding.root
    }

    private fun init() {
        binding.setOnSearchClick {
            inputs()
        }

        binding.setOnLogOutClick {
            auth.signOut()
            findNavController().navigate(R.id.action_searchFragment_to_authenticationFragment)
        }
    }



    private fun inputs() {
        val artist = binding.artist.toString()
        val song = binding.song.toString()
        if (artist.isNotEmpty() && song.isNotEmpty()){
            viewModel.initAdvice(artist, song)
            observe(artist, song)
        }else{
            Toast.makeText(requireContext(), "Please fill all fields", Toast.LENGTH_SHORT).show()
        }
    }



    private fun observe(artist: String, song: String) {
        viewModel._lyricLiveData.observe(viewLifecycleOwner, {
            when (it.status) {
                ResultControl.Status.SUCCESS -> {
                    binding.visible = it.refreshing
                    val bundle = bundleOf("Band" to artist, "Song" to song, "Lyric" to it.data!!)
                    findNavController().navigate(R.id.action_searchFragment_to_lyricFragment, bundle)
                }
                ResultControl.Status.ERROR -> {
                    Toast.makeText(requireContext(), "${it.message}", Toast.LENGTH_SHORT).show()
                    binding.visible = it.refreshing
                }
                ResultControl.Status.LOADING -> {
                    binding.visible = it.refreshing
                }
            }
        })
    }



    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}