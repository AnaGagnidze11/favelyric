package com.example.favelyric.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.example.favelyric.R
import com.example.favelyric.databinding.FragmentLogInBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class LogInFragment : Fragment() {

    private var _binding: FragmentLogInBinding? = null
    private val binding get() = _binding!!

    private lateinit var auth: FirebaseAuth

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DataBindingUtil.inflate(inflater, R.layout.fragment_log_in, container, false)
        init()
        return binding.root
    }

    private fun init(){
        auth = Firebase.auth
        binding.setOnLILogInClick {
            binding.visible = true
            logIn()
        }

    }

    private fun logIn(){
        val email = binding.email.toString()
        val password = binding.password.toString()


        if (email.isNotEmpty() && password.isNotEmpty()){
            if (android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                binding.signUpEmailLayout.error = null
                auth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(requireActivity()) { task ->
                        if (task.isSuccessful) {
                            binding.visible = false
                            findNavController().navigate(R.id.action_logInFragment_to_searchFragment)
                        } else {
                            toastErrors("Authentication failed.")
                        }
                    }
            }else{
                binding.visible = false
                binding.signUpEmailLayout.error = "Email format is incorrect"
            }
        }else{
            toastErrors("Please fill all fields")
        }
    }

    private fun toastErrors(toastText: String){
        binding.visible = false
        Toast.makeText(requireContext(), toastText, Toast.LENGTH_SHORT).show()
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}