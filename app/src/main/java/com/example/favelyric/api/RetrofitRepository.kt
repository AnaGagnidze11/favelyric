package com.example.favelyric.api

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface RetrofitRepository {

    @GET("/v1/{artist}/{song}")
    suspend fun getLyric(
        @Path("artist") artist: String,
        @Path("song") song: String
    ): Response<LyricModel>
}