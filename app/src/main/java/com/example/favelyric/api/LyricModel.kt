package com.example.favelyric.api

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class LyricModel(val lyrics: String): Parcelable
