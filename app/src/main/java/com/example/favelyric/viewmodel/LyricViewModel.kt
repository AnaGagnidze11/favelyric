package com.example.favelyric.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.favelyric.api.LyricModel
import com.example.favelyric.api.ResultControl
import com.example.favelyric.api.RetrofitService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class LyricViewModel: ViewModel() {

    private val lyricLiveData = MutableLiveData<ResultControl<LyricModel>>().apply {
        mutableListOf<LyricModel>()
    }
    val _lyricLiveData: LiveData<ResultControl<LyricModel>> = lyricLiveData

    fun initAdvice(artist: String, song: String){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                getAdvice(artist, song)
            }
        }
    }

    private suspend fun getAdvice(artist: String, song: String){
        lyricLiveData.postValue(ResultControl.loading(true))
        val result = RetrofitService.retrofitService.getLyric(artist, song)
        if (result.isSuccessful){
            val lyric = result.body()
            lyric?.let {
                lyricLiveData.postValue(ResultControl.success(lyric))
            }

        }else{
                lyricLiveData.postValue(ResultControl.error(result.message()))
        }
    }
}